import urllib.request

import cv2
import numpy as np

def get_image_from_url(image_url: str, reading_mode=cv2.IMREAD_GRAYSCALE):
    """
    Download image from the given url, decode and return it (in grayscale by default).

    :param image_url: url path to the image
    :param reading_mode: Image reading mode. Default is cv2.IMREAD_GRAYSCALE
    :returns: cv2 image object
    """
    url_response = urllib.request.urlopen(image_url)
    return cv2.imdecode(
        np.array(bytearray(url_response.read()), dtype=np.uint8), reading_mode
    )


def get_cleaned_hough_lines(canny_edges):
    """
    Cyclically finds lines on the image using cv2.HoughLines function. After finding the lines, paints the first one
    with black and so on until there are no lines left. Thanks to this, all segments found on one straight line will be
    counted as one line.

    :param canny_edges: Image with edges
    :returns: Found lines as list in the format that returns cv2.HoughLines function
    """
    working_edges = canny_edges.copy()
    total_lines = []
    while True:
        lines = cv2.HoughLines(working_edges, 1, np.pi / 360, 100)

        if lines is None:
            break
        else:
            found_line = lines[0]
            total_lines.append(found_line)

            rho, theta = found_line[0]
            a = np.cos(theta)
            b = np.sin(theta)
            x0 = a * rho
            y0 = b * rho
            x1 = int(x0 + 1000 * -b)
            y1 = int(y0 + 1000 * a)
            x2 = int(x0 - 1000 * -b)
            y2 = int(y0 - 1000 * a)
            cv2.line(working_edges, (x1, y1), (x2, y2), 0, 3)

    return total_lines


def get_parallel_lines_count(hough_lines):
    existing_angles = {}
    for angle in [line[0][-1] for line in hough_lines]:
        for existing_angle, count in existing_angles.items():
            if abs(angle - existing_angle) < 0.05:
                existing_angles[existing_angle] += 1
                break
        else:
            existing_angles[round(angle, 5)] = 1
    parallel_lines_count = 0
    for angle_count in existing_angles.values():
        if angle_count > 1:
            parallel_lines_count += 1
    return parallel_lines_count


# url = input()
# img = get_image_from_url(url, cv2.IMREAD_UNCHANGED)
img = cv2.imread("Tracking_robot_way/Data/Images/Sample_image_1.jpg")

# Convert the image to grayscale
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

# Blur the image
blur = cv2.bilateralFilter(gray, 12, 120, 140)

# Find edges in the image
edges = cv2.Canny(blur, 80, 180, apertureSize=3)

dilated = cv2.dilate(edges, None, iterations=1)

# Find contours
contours_, hierarchy = cv2.findContours(dilated, cv2.RETR_CCOMP, cv2.CHAIN_APPROX_SIMPLE)
contours = []

contour = None
biggest_area = 0
for cntr in contours_:
    cntr_area = cv2.contourArea(cntr)
    if cntr_area > 1000:
        contours.append(cntr)
        if cntr_area > biggest_area:
            biggest_area = cntr_area
            contour = cntr

# Approximate the contour
contour_approx = cv2.approxPolyDP(contour, 0.003 * cv2.arcLength(contour, True), True)

# Find circles in the image
# circles = cv2.HoughCircles(edges, cv2.HOUGH_GRADIENT, 1, 1000, param1=50, param2=30, minRadius=0, maxRadius=0)

lines = get_cleaned_hough_lines(edges)
lines_count = len(lines)
parallel_lines_count = get_parallel_lines_count(lines)

# Sphere or Tor, if there are no lines on the image
if lines_count == 0:
    # Tor, if several contours are found
    if len(contours) // 2 >= 2:
        print('Tor')
    # Sphere otherwise
    else:
        print('Sphere')
# Arch, if contour is not convex
elif not cv2.isContourConvex(contour_approx):
    print('Arch')
# Cone or Cylinder, if there are exactly two lines
elif lines_count == 2:
    # Cone, if these lines are not parallel
    if parallel_lines_count == 0:
        print('Cone')
    # Cylinder otherwise
    else:
        print('Cylinder')
# Remained Cube or Pyramid
else:
    if lines_count >= 8 or parallel_lines_count >= 2:
        print('Cube')
    else:
        print('Pyramid')
