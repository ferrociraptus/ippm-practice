from curses.ascii import ESC

# подключаем библиотеку numpy для работы с матрцами и opencv для обработки изображений
import cv2 as cv
import numpy as np

# создаём объект для считывания видеопотока 
cap = cv.VideoCapture("resources_videos/cube.mp4")

# заводим лист, который нам понадобится для хранения координат центральной точки 
# объекта и отображения траектории движения соответственно
track_points = []


def empty(a):
    pass


# заводим название для окна отображения
cv.namedWindow("HSV")

# обрабатываем видепоток
while cap.isOpened():
    # считываем покадрово 
    success, frame = cap.read()

    # если не удалось извлечь кадр, то останавливаем работу 
    if not success:
        break

    # изменение размера изображения.
    rescale_kf = 40
    dim = [int(frame.shape[1] * rescale_kf / 100), int(frame.shape[0] * rescale_kf / 100)]
    rs_frame = cv.resize(frame, dim, interpolation=cv.INTER_AREA)  # rescaled frame

    # выполняем перевод цветовой гаммы BGR в HSV
    hsv = cv.cvtColor(rs_frame, cv.COLOR_BGR2HSV)

    # параметры подобраны с помощью заготовленных трекбаров из интерфейсов high GUI opencv
    hsv_min = np.array([70, 88, 26])
    hsv_max = np.array([182, 255, 153])
    # создаем маску для выделения нужного цвета
    thresh = cv.inRange(hsv, hsv_min, hsv_max)

    cv.imshow("mask", thresh)

    # находим контуры объекта 
    contours, hierarchy = cv.findContours(thresh.copy(), cv.RETR_EXTERNAL, cv.CHAIN_APPROX_NONE)

    for cnt in contours:

        # для найденных контуров вписываем прямоугольник
        rect = cv.minAreaRect(cnt)  # возвращает структуру ,содержащую ( center (x,y), (width, height), angle of
        # rotation )

        # находим площадь вписанного прямоугольника
        area = int(rect[1][0] * rect[1][1])  # произведение ширины на высоту прямоугольника
        print(rect)

        # если площадь удовлетворяет условию
        if area > 500:
            # находим вершины прямоугольника и округляем их координаты
            box = cv.boxPoints(rect).astype(np.int0)
            # записываем координаты центра прямоугольника
            center = (int(rect[0][0]), int(rect[0][1]))
            print("center", center)
            # добавляем в начало нашего листа координаты центра
            track_points.insert(0, center)

            # рисуем контур найденного прямоугольника
            cv.drawContours(rs_frame, [box], 0, (0, 255, 0), 2)
            # в центре прямоугольника рисуем окружность
            cv.circle(rs_frame, center, 3, (255, 0, 255), 3)
            # выводим координаты центра объекта относительно изображения
            cv.putText(rs_frame, str(center), center, cv.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 255), 2)

            # удаляем последние точки из листа, чтобы не загромождать картинку
            track_points = track_points[:150]
            # Рисуем траекторию движения центра отслеживаемого объекта
            for i in range(1, len(track_points)):
                cv.line(rs_frame, track_points[i], track_points[i - 1], (255, 0, 255), 2)

    # выводим обработанный видеопоток
    cv.imshow("Tracking", rs_frame)
    # ожидаем нажатие клавиши с клавиатуры
    ch = cv.waitKey(30)
    # проверяем , если клавиша esc , то завершаем работу программы
    if ch == ESC:
        break

# освобождаем видеопоток и закрываем окна
cap.release()
cv.destroyAllWindows()
