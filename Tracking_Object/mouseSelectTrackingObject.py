from curses.ascii import ESC

import cv2 as cv

# Различные варианты трекеров

# tracker = cv.legacy.TrackerBoosting_create()
# tracker = cv.legacy.TrackerMIL_create()
# tracker = cv.legacy.TrackerKCF_create()
# tracker = cv.legacy.TrackerTLD_create()
tracker = cv.legacy.TrackerMedianFlow_create()
# tracker = cv.legacy.TrackerCSRT_create()
# tracker = cv.legacy.TrackerMOSSE_create()

# Создаём объект для дальнейшей обработки видеопотока
cap = cv.VideoCapture("resources_videos/cube.mp4")
# cap = cv.VideoCapture(0) - если хотим принимать видеопоток с камеры


# Создаем лист для хранения центральных точек отслеживаемого объекта
track_points = []
delete = 0

# Обработка видеопотока
while cap.isOpened():
    # FPS counter
    timer = cv.getTickCount()

    # Считываем по кадру
    success, frame = cap.read()

    # Изменяем масштаб выводящегося на экран видео.
    rescale_kf = 40
    dim = [int(frame.shape[1] * rescale_kf / 100), int(frame.shape[0] * rescale_kf / 100)]
    rf = cv.resize(frame, dim, interpolation=cv.INTER_AREA)  # rescaled frame

    # Ожидаем нажатия клавиши с клавиатуры, если нажата t, то инициализируем трекер
    if cv.waitKey(5) == ord('t'):
        # Мышкой выделяем объект , который будем отслеживать
        bounding_box = cv.selectROI("Tracking", rf, False)
        # Инициализируем трекинг объекта
        tracker.init(rf, bounding_box)

    success, bounding_box = tracker.update(rf)

    if success:
        # Отслеживаемый объект обводим в прямоугольник
        x, y, w, h = [int(i) for i in bounding_box]
        cv.rectangle(rf, (x, y), ((x + w), (y + h)), (0, 255, 0), 3, 3)
        cv.putText(rf, "Tracking", (10, 20), cv.FONT_HERSHEY_SIMPLEX, 0.7, (0, 255, 0), 2)

        center = (x + w // 2, y + h // 2)
        cv.circle(rf, center, 5, (255, 0, 255), 10, cv.FILLED)
        track_points.insert(0, center)

        # выводим координаты центра отслеживаемого объекта относительно изобрадения на экране
        cv.putText(rf, str(center), center, cv.FONT_HERSHEY_SCRIPT_SIMPLEX, 0.8, (0, 250, 250), 2)

        # удаляем последние точки из листа, чтобы не загромождать картинку
        track_points = track_points[:150]
        # Рисуем траекторию движения центра отслеживаемого объекта
        for i in range(1, len(track_points)):
            cv.line(rf, track_points[i], track_points[i - 1], (255, 0, 255), 2)

    else:
        # Выводим текст на видео
        cv.putText(rf, "there is no tracking obj", (10, 20), cv.FONT_HERSHEY_SIMPLEX, 0.7, (0, 0, 255), 2)
        track_points.clear()

    FPS = cv.getTickFrequency() / (cv.getTickCount() - timer)

    cv.putText(frame, str(int(FPS)), (10, 20), cv.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)

    # Выводим выходящий поток с трекингом оъекта
    cv.imshow("Tracking", rf)

    # Останавливаем работу при нажатии кнопки esc
    ch = cv.waitKey(5)  # ожидаем нажатие клавиши в течение 5 мс
    if ch == ESC:  # если это была клавиша ESC, то выходим из бесконечного цикла
        break

cap.release()
cv.destroyAllWindows()
