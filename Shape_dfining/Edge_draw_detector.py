import cv2 as cv
import numpy as np
from random import randint

# Путь (относительный или полный) до изображения
filename = "Images/Cone.png"

# Прочтение изображения
image = cv.imread(filename, cv.IMREAD_GRAYSCALE)

# Проверка корректности чтения файла
if image is None:
    print('Error opening image!')
    exit(-1)

# Применяем детектор границ Кэлли
dst = cv.Canny(image, 50, 200, None, 3)

# Создаём 2 копии цветного изображения для применения 2 различных детекторов линий
cdst = cv.cvtColor(dst, cv.COLOR_GRAY2BGR)
cdstP = np.copy(cdst)

# Находим линии первым способом
lines = cv.HoughLines(dst, 1, np.pi / 180, 150, None, 0, 0)

# И отрисовываем их, если линии были найдены
if lines is not None:
    for i in range(0, len(lines)):
        rho = lines[i][0][0]
        theta = lines[i][0][1]
        a = np.cos(theta)
        b = np.sin(theta)
        x0 = a * rho
        y0 = b * rho
        pt1 = (int(x0 + 1000 * (-b)), int(y0 + 1000 * (a)))
        pt2 = (int(x0 - 1000 * (-b)), int(y0 - 1000 * (a)))
        cv.line(cdst, pt1, pt2, (0, 0, 255), 3, cv.LINE_AA)

# Находим линии 2ым способом и отрисовываем произвольным цветом
linesP = cv.HoughLinesP(dst, 1, np.pi / 180, 50, None, 50, 10)
if linesP is not None:
    for i in range(0, len(linesP)):
        line = linesP[i][0]
        cv.line(cdstP, (line[0], line[1]), (line[2], line[3]), (randint(0, 255), randint(0, 255), randint(0, 255)), 3,
                cv.LINE_AA)

# Отрисовываем изображения
cv.imshow("Source", image)
cv.imshow("Detected Lines - Standard Hough Line Transform", cdst)
cv.imshow("Detected Lines - Probabilistic Line Transform", cdstP)

# Ожидаем нажатие произвольной клавиши, после чего завершаем выполнение
cv.waitKey(0)
cv.destroyAllWindows()
