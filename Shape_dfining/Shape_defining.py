import cv2 as cv
import numpy as np


def get_parallel_lines_count(hough_lines, delta=0.05):
    """
    :param hough_lines: список линий, определённых с помощью cv.HoughLines
    :param delta: погрешность сравнения углов
    :return: количество наборов прямых (каждый набор не менее 2 прямых) параллельных между собой
    """
    existing_angles = {}

    # Алгоритм: считаем колличество одинаковых углов наклона прямых на изображении,
    for angle in [line[0][-1] for line in hough_lines]:
        for existing_angle, count in existing_angles.items():
            if abs(angle - existing_angle) < delta:
                existing_angles[existing_angle] += 1
                break
        else:
            existing_angles[round(angle, 3)] = 1

    # если таких углов > 2, то значит мы нашли такой набор.
    parallel_lines_count = 0
    for angle_count in existing_angles.values():
        if angle_count > 1:
            parallel_lines_count += 1
    return parallel_lines_count


# Путь (относительнй или полный) файла изображения
filename = "Images/Arch.png"

# Считываем изображение
image = cv.imread(filename, cv.IMREAD_GRAYSCALE)

# Проверяем открылся ли файл
if image is None:
    print('Error opening image!')
    exit(-1)

# Применяем детектор границ Кэнни
dst = cv.Canny(image, 50, 200, None, 3)
cv.imshow("Canny detection", dst)

# Переводим изображение из одноканального (ЧБ) в 3 канальное (BGR),
# чтобы изобразить цветное изображение
cdst = cv.cvtColor(image, cv.COLOR_GRAY2BGR)

# Ищем линии на изображении и отрисовываем их
lines = cv.HoughLines(dst, 1, np.pi / 180, 150, None, 0, 0)
if lines is not None:
    for i in range(0, len(lines)):
        rho = lines[i][0][0]
        theta = lines[i][0][1]
        a = np.cos(theta)
        b = np.sin(theta)
        x0 = a * rho
        y0 = b * rho
        pt1 = (int(x0 + 1000 * (-b)), int(y0 + 1000 * (a)))
        pt2 = (int(x0 - 1000 * (-b)), int(y0 - 1000 * (a)))
        cv.line(cdst, pt1, pt2, (0, 0, 255), 3, cv.LINE_AA)

# "Расширяем" белые границы изображения и ищем контур
dilated = cv.dilate(dst, None, iterations=1)
contours, _ = cv.findContours(dilated, cv.RETR_EXTERNAL, cv.CHAIN_APPROX_SIMPLE)
# Дополнительно аросимируем контур и
contours = [cv.approxPolyDP(contour, 0.002 * cv.arcLength(contour, True), True)
            for contour in contours if cv.contourArea(contour) > 12000]

# выбираем самый контур с самой большой площадью,
# предполагая, что это контур необходимой фигуры
contour = max(contours, key=lambda x: cv.contourArea(x))
cv.drawContours(cdst, np.array([contour]), -1, (200, 0, 0), 2)

# Определяем колличество обнаруженных линий
# и колличество наборов параллельных линий
lines_count = len(lines)
parallel_lines_count = get_parallel_lines_count(lines)

shape_name = "None"
# Если ни одной линии на изображении нет,
# то предполагаем, что это тор или сфера.
if lines_count == 0:
    # Если контура вложенные, то скорее всего жто тор,
    # т.е. м обнаружили отверстие
    if len(contours) // 2 >= 2:
        shape_name = 'Tor'
    # Иначе это сфера, действуем методом исключения,
    # опираясь на список наших фигур
    else:
        shape_name = 'Sphere'
# Если контур вогнутый, то скорее всего это арка.
elif not cv.isContourConvex(contour):
    shape_name = 'Arch'
# Если мы обнаружили только 2 прямые, то это или цилиндр, или конус
elif lines_count == 2:
    # Если линии не параллельны, то это конус
    if parallel_lines_count == 0:
        shape_name = 'Cone'
    # Иначе цилиндр
    else:
        shape_name = 'Cylinder'
# Теперь определим куб или пирамиду, по количеству рёбер(линий) и
# количеству наборов параллельных прямых
else:
    if lines_count >= 8 or parallel_lines_count >= 2:
        shape_name = 'Cube'
    else:
        shape_name = 'Pyramid'

# Найдём центр контура, это будет точка центра текста с подписью формы
text_point = np.mean(contour, axis=0).astype(np.int64)[0]

# Найдём размер надписи, чтобы узнать на сколько нужно сместить текст,
# чтобы его центр совпал с центром детали
font = cv.FONT_HERSHEY_SIMPLEX
text_size = cv.getTextSize(shape_name, font, 2, 2)[0]

# Находим точку (левый нижний угол) текста, чтобы на
# исходном изображении текст был ровно по центру фигуры
text_point[0] = text_point[0] - text_size[0] // 2
text_point[1] = text_point[1] + text_size[1] // 2
text_point = tuple(text_point)

# Рисуем текст на исходном изображении
cv.putText(image, shape_name, text_point, font, 2, (0, 40, 0), 2, cv.LINE_AA)
cv.putText(cdst, shape_name, text_point, font, 2, (0, 40, 0), 2, cv.LINE_AA)

# Показываем результат
cv.imshow("Source", image)
cv.imshow("Detected Lines (in red) - Standard Hough Line Transform", cdst)

# Ждём нажатие любой клавиши и после этого завершаем работу
cv.waitKey(0)
cv.destroyAllWindows()
