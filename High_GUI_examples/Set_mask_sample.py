import cv2 as cv
import numpy as np


# Cоздаем пустую функцию
def nothing(*arg):
    pass


# Создаем окно с результатом и окно настроек
cv.namedWindow("Result", cv.WINDOW_NORMAL)
cv.namedWindow("HSV mask")
cv.namedWindow("Picture")

# Открываем фотографию и записываем в переменную img.
# В переменную colors записываем картинку с цветовым спектром HVS
img = cv.imread("Photos/apple.jpg")
colors = cv.imread("Photos/colors.png")

# Конвертируем в цветовую модель HSV
hsv = cv.cvtColor(img, cv.COLOR_BGR2HSV)
hsv_colors = cv.cvtColor(colors, cv.COLOR_BGR2HSV)

# создаем 6 бегунков для настройки начального и конечного цвета фильтра
cv.createTrackbar('Min H', 'HSV mask', 0, 180, nothing)
cv.createTrackbar('Min S', 'HSV mask', 0, 255, nothing)
cv.createTrackbar('Min V', 'HSV mask', 0, 255, nothing)
cv.createTrackbar('Max H', 'HSV mask', 180, 180, nothing)
cv.createTrackbar('Max S', 'HSV mask', 255, 255, nothing)
cv.createTrackbar('Max V', 'HSV mask', 255, 255, nothing)

while True:

    # считываем значения бегунков
    h1 = cv.getTrackbarPos('Min H', 'HSV mask')
    s1 = cv.getTrackbarPos('Min S', 'HSV mask')
    v1 = cv.getTrackbarPos('Min V', 'HSV mask')
    h2 = cv.getTrackbarPos('Max H', 'HSV mask')
    s2 = cv.getTrackbarPos('Max S', 'HSV mask')
    v2 = cv.getTrackbarPos('Max V', 'HSV mask')

    # формируем начальный и конечный цвет фильтра
    h_min = np.array((h1, s1, v1))
    h_max = np.array((h2, s2, v2))

    # накладываем фильтр на кадр в модели HSV
    thresh = cv.inRange(hsv, h_min, h_max)
    thresh_colors = cv.inRange(hsv_colors, h_min, h_max)
    thresh_colors = cv.bitwise_and(colors, colors, mask=thresh_colors)

    # вывести кадр в окно
    cv.imshow('Result', thresh)
    cv.imshow('HSV mask', thresh_colors)
    cv.imshow("Picture", img)

    # Выход при нажатии esc
    k = cv.waitKey(27)
    if k != -1:
        break

cv.destroyAllWindows()
