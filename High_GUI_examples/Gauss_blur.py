import cv2 as cv


def nothing(*arg):
    pass


# создаем окно настроек
cv.namedWindow("Gauss blur", cv.WINDOW_NORMAL)

# Открываем фотографию и записываем в переменную img
img = cv.imread("Photos/apple.jpg")

# Создаем полузнки для ядра размытия, сигмы и ползунок ключения размытия
cv.createTrackbar('kernel', 'Gauss blur', 1, 25, nothing)
cv.createTrackbar('sigmaX', 'Gauss blur', 0, 25, nothing)
cv.createTrackbar('sigmaY', 'Gauss blur', 0, 25, nothing)
cv.createTrackbar("turn on", 'Gauss blur', 0, 1, nothing)
while True:

    # считываем значения
    kernel = cv.getTrackbarPos('kernel', 'Gauss blur')
    sigmaX = cv.getTrackbarPos('sigmaX', 'Gauss blur')
    sigmaY = cv.getTrackbarPos('sigmaY', 'Gauss blur')
    isBlur = cv.getTrackbarPos('turn on', 'Gauss blur')

    # Значения ядра должны быть нечетными
    if kernel % 2 == 0:
        kernel += 1

    # Применяем размытие по Гауссу
    blur = cv.GaussianBlur(img, (kernel, kernel), sigmaX=sigmaX, sigmaY=sigmaY)

    # Проверка, включено ли размытие
    if isBlur == 1:
        cv.imshow('Gauss blur', blur)
    else:
        cv.imshow('Gauss blur', img)

    # выход из программы по нажатию на esc
    k = cv.waitKey(27)
    if k != -1:
        break

cv.destroyAllWindows()
