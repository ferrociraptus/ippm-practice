import cv2 as cv

# Создаем глобальные переменные
draw = False
LeftTop, RightBottom = (0, 0), (0, 0)


# Функция-обработчик движения мыши должна принимать 5 определенных значений:
# event - событие, x,y - текущие координаты мыши. Остальные параметры нам не понадобятся
def Draw_rect(event, x, y, flagval, par):
    global draw, LeftTop, RightBottom
    if event == cv.EVENT_LBUTTONDOWN:
        draw = True
        # Запоминаем координаты 1-го нажатия
        LeftTop = (x, y)
        RightBottom = (x, y)

    elif event == cv.EVENT_MOUSEMOVE:
        # Если перед этим нажали на ЛКМ, запоминаем координаты мыши
        if draw == True:
            RightBottom = (x, y)

    elif event == cv.EVENT_LBUTTONUP:
        draw = False
        RightBottom = (x, y)


# создаем окно
cv.namedWindow("Rect", cv.WINDOW_NORMAL)

# Установим функцию Draw_rect как обработчик движения мыши
cv.setMouseCallback("Rect", Draw_rect)

while True:

    # Открываем фотографию и записываем в переменную img
    img = cv.imread("Photos/apple.jpg")

    # Рисуем прямоугольник зелёного цвета по заданным координатам
    cv.rectangle(img, LeftTop, RightBottom, (0, 255, 0), thickness=2)
    cv.imshow('Rect', img)

    k = cv.waitKey(27)
    if k != -1:
        break

cv.destroyAllWindows()
