Данная директория содержит кодовую базу примеров для методички по OpenCV (ROBBO)


###### Требования написания кода:

1. К каждой написанной функции или методу должен прилагаться краткий
   комментарий по принципам работы и входным параметрам. (исключением
   являются магичиские методы)
2. Во всех методах и функциях необходимо прописывать типизацию Python. 
   (исключением являются магичиские методы)
3. При использовании неоправданно запутанных конструкций(костылей)
   необходимо писать комментарий-пояснение.
   
###### Договоренности написания кода:
1. Перед реализацией своей функции/класса сначала необходимо
   удостовериться в отсутсвии данного функционала в уже реализованном
   софте.
--------
