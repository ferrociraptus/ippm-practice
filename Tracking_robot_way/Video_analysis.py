import cv2 as cv
from os import listdir
import numpy as np

VIDEOS_DIR = "Data/Videos"  # пишем капсом, чтобы обозначить "константу"

# получаем список файлов из директории с видеофайлами
videos_path = listdir(VIDEOS_DIR)

# выводим список доступных видеофайлов
print("Available videos:", *["\t" + str(i) + ")" + path for i, path in enumerate(videos_path)], sep="\n")

# составляем список полных путей до видео от текущего местоположения запуска скрипта
videos_path = [VIDEOS_DIR + "/" + path for path in videos_path]

video_path = videos_path[0]  # возьмём какое-нибудь конкретное видео.

# создадим окно в котором будем отображать обработанный видеовыход
cv.namedWindow("Video tracking")

# создадим объект через который мы будем читать видеопоток
capture = cv.VideoCapture()
capture.open(video_path)

# обрабатываем видепоток
while capture.isOpened():
    ret, image = capture.read()

    # если поптка извлечения кадра была удачной
    if ret is True:
        image = cv.resize(image, (640, 480))  # сжимаем исходное изображение
        hsv_image = cv.cvtColor(image, cv.COLOR_BGR2HSV)  # переводим цветовую гамму BGR в HSV

        # эксперементальным путём подбираем коэффициенты цветового диапазона так,
        # чтобы необходимый нам объект был наиболее ярко выражен
        h_min = np.array((0, 0, 0), np.uint8)
        h_max = np.array((200, 250, 95), np.uint8)

        # применеям фильтр
        image_extraction = cv.inRange(hsv_image, h_min, h_max)

        # произведём поиск контуров по отсортированному изображению
        # и дополнитеьно апроксимизируем поверхность, одновременно отсеивая
        # слишком маленькие площади контуров (которые однозначно нам не подходят)
        contours, _ = cv.findContours(image_extraction, cv.RETR_EXTERNAL, cv.CHAIN_APPROX_SIMPLE)
        contours = [cv.approxPolyDP(contour, 0.002 * cv.arcLength(contour, True), True)
                    for contour in contours if cv.contourArea(contour) > 12000]

        # нарисуем и изобразим контура на исходном изображении
        cv.drawContours(image, contours, -1, (0, 100, 0), 2)

        # найдём середину изображения, относительно которой будем измерять отклонение от маршрута
        # цетр будем искать отталкиваясь от размеров исходного изображения
        middle = np.array([i // 2 for i in image.shape[:2]][::-1])

        # обозначим центр на исходном изображении с помощью красного кружочка
        cv.circle(image, middle, 5, (0, 0, 255), 2)

        # пройдём все точки прямой, проходящей через середину изображения
        # и найдём точки пересечения данной прямой с контуром "дороги".

        # отрисуем данную линию на исходном изображении
        cv.line(image, (0, middle[1]), (middle[0] * 2, middle[1]), (255, 0, 0), 1)

        # поиск точек пересечения с помощью перебора точек на заданной
        # приямой и проверки нахождения точки на контуре траекории.
        intersection_lines = []
        x = 0
        y = int(middle[1])
        while x < middle[0] * 2:
            for contour in contours:
                if cv.pointPolygonTest(contour, (x, y), False) >= 0:
                    first_point = np.array([x, y])
                    while True:
                        if cv.pointPolygonTest(contour, (x, y), False) < 0:
                            intersection_lines.append((first_point, np.array([x - 1, y])))
                            break
                        x += 1
            x += 1

        # отрисуем найденые отрезки пересечения и найдём их центры, т.е. точки куда необходимо провернуть.
        # если таких точек несколько выберем среди них одну, наиболее близкую к центральной точке.
        arrival_point = middle
        min_size = middle[0] * 2
        for line in intersection_lines:
            cv.line(image, line[0], line[1], (150, 0, 150), 2)
            cv.circle(image, line[0], 3, (255, 0, 0), 2)
            cv.circle(image, line[1], 3, (255, 0, 0), 2)
            section_middle = (line[0] + line[1]) // 2
            cv.circle(image, section_middle, 3, (150, 150, 0), 2)

            distance = np.abs(middle[0] - section_middle[0])
            if distance < min_size:
                min_size = distance
                arrival_point = section_middle

        # отрисуем точку прибытия
        cv.circle(image, arrival_point, 7, (0, 255, 0), 1)

        # для определения угла отклонения возьмём вектор,
        # проходящий по оси ординат до центральной точки и
        # вектор до точки прибытия. Угол между прямыми определим через
        # нахождение косинуса из скалярного произведения векторов.

        # сначала найдём 0 (точку начала отсчёта) и изобразим вектора,
        # относительно которых будем искать угол
        zero_point = middle.copy()
        zero_point[1] *= 2

        central_vector = np.array([zero_point, middle])
        arrival_vector = np.array([zero_point, arrival_point])

        # изобразим данные вектора
        cv.arrowedLine(image, *central_vector, (10, 10, 155), 2, cv.LINE_AA, tipLength=0.075)
        cv.arrowedLine(image, *arrival_vector, (10, 155, 10), 2, cv.LINE_AA, tipLength=0.075)

        # найдём косинус угла
        central_rad_vector = central_vector[1] - central_vector[0]  # радиус-вектор точки центра
        arrival_rad_vector = arrival_vector[1] - arrival_vector[0]  # радиус-вектор точки прибытия
        angle_cos = np.vdot(central_rad_vector, arrival_rad_vector) / \
                    (np.linalg.norm(central_rad_vector) * np.linalg.norm(arrival_rad_vector))

        # найдём исходный угол в радианах и переведём в градусы
        angle_rad = np.arccos(angle_cos)
        angle_rad *= -1 if middle[0] - arrival_point[0] < 0 else 1
        angle_grad = round(angle_rad * (180 / np.pi), 2)
        print("\nAngle:", angle_grad)

        # впишем градусную меру по середине между веторами
        # для начала найдём точку, куда необходимо вписать текст
        text_point = (np.sum(central_vector, axis=0) + np.sum(arrival_vector, axis=0)) // 4

        font = cv.FONT_HERSHEY_SIMPLEX
        text_size = cv.getTextSize(str(angle_grad), font, 0.75, 2)[0]

        # находим точку (левый нижний угол) текста, чтобы на
        # исходном изображении текст был ровно между векторами
        text_point[0] = text_point[0] - text_size[0] // 2
        text_point[1] = text_point[1] + text_size[1] // 2
        cv.putText(image, str(angle_grad), text_point, font, 0.75, (0, 40, 0), 2, cv.LINE_AA)

        # показываем обработанныый кадр
        cv.imshow("Video tracking", image)

        # ждём нажатие любой клавиши в течении 30мс, если что-то было нажато,
        # то завершаем работу
        if cv.waitKey(30) >= 0:
            break

    else:
        break

capture.release()  # закрываем видеопоток
cv.destroyAllWindows()  # завершаем работу всех графических окон
