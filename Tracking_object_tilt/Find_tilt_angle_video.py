# подключаем библиотеки для работы с
# матрицами, мат. функциями (numpy) и изображениями (openCV)
import numpy as np
import cv2 as cv


# пишем функцию, которая находит угол между векторами v1 и v2 в плоскости
def findAngle(v1, v2):
    # по определению скалярного произведения находим косинус угла, обратная функция арккосинус
    # возвращает угол в радианах, поэтому переводим его в градусы
    return 180.0 / np.pi * np.arccos((v1[0] * v2[0] + v1[1] * v2[1]) / (cv.norm(v1) * cv.norm(v2)))


# постоянные
BLUE = (255, 0, 0)  # синий цвет в формате BGR
GREEN = (0, 255, 0)  # зеленый цвет в формате BGR
HOR_VEC = (1, 0)  # вектор, направленный вдоль горизонта картинки
ESC = 27

# устанавливаем границы hsv фильтра, найденные с помощью скрипта Set_mask_sample.py
hsv_min = np.array((0, 163, 140), np.uint8)
hsv_max = np.array((13, 255, 255), np.uint8)
# np.array принимает строку значений и формат чисел (здесь безнаковые числа от 0 до 255), и возвращает матрицу-строку

# открываем видеопоток. Аргумент 0, если вебкамера, 1, если камера с usb
cpt = cv.VideoCapture(0)

# проверим, успешно ли открылся видеопоток
if not cpt.isOpened():
    print('OpenVideoCaptureError!')
    exit(-1)

# пока не выйдем из цикла (возможно выйти при нажатии клавиши ESC)
while True:
    flag, frame = cpt.read()  # читаем кадр видеопотока в frame
    if not flag:  # если кадр не прочелся, то печатаем 'FrameError!' и выходим из цикла
        print('FrameError!')
        break
    else:
        frame = cv.flip(frame, 1)  # отразить картинку относительно Oy (второй аргумент 1). Это необходимо,
        # поскольку в видеопотоке кадры изначально отзеркалены

        # меняем цветовую модель кадра с BGR на HSV для выделения контура
        hsv = cv.cvtColor(frame, cv.COLOR_BGR2HSV)
        # применяем цветовой фильтр
        thresh = cv.inRange(hsv, hsv_min, hsv_max)

        # ищем контуры, сохраняем их и иерархию (здесь она не пригодится) в соответствующие переменные.
        # RETR_EXTERNAL - только крайние внешние контуры, CHAIN_APPROX_NONE - упаковка контура отсутствует
        contours, hierarchy = cv.findContours(thresh.copy(), cv.RETR_EXTERNAL, cv.CHAIN_APPROX_NONE)

        for cnt in contours:  # для каждого найденного контура
            rect = cv.minAreaRect(cnt)  # пытаемся вписать прямоугольник
            area = int(rect[1][0] * rect[1][1])  # ищем площадь прямоугольника

            if area > 1000:  # если она достаточно большая, то
                box = cv.boxPoints(rect)  # ищем вершины прямоугольника
                box = np.int0(box)  # округляем координаты
                center = (int(rect[0][0]), int(rect[0][1]))  # записываем координаты центра прямоугольника

                # ищем векторы смежных сторон прямоугольника
                vec1 = np.int0((box[1][0] - box[0][0], box[1][1] - box[0][1]))
                vec2 = np.int0((box[2][0] - box[1][0], box[2][1] - box[1][1]))

                # находим из них наиболее длинный
                vec = vec1
                if cv.norm(vec2) > cv.norm(vec1):
                    vec = vec2

                angle = findAngle(HOR_VEC, vec)  # ищем угол между высотой цилиндра и горизонтом

                # найдем точки, к которым нужно провести отрезки угла
                p1 = (center[0] + vec[0] // 2, center[1] + vec[1] // 2)  # ищем середину верхнего отрезка прямоугольника
                # Для этого мы к вектору center прибавили половину вектора высоты цилиндра

                p2 = center  # присваиваем второй точке значение центральной
                while cv.pointPolygonTest(box, p2, False) == 1:  # пока точка p2 находится внутри прямоугольника
                    p2 = (p2[0] + 1, p2[1])  # смещать её вдоль горизонта вправо

                # на кадр frame
                cv.drawContours(frame, [box], 0, BLUE, 2)  # рисуем синий прямоугольник box толщины 2
                cv.circle(frame, center, 2, GREEN, 2)  # рисуем зеленую окружность в центре center радиуса 5 толщины 2
                cv.line(frame, center, p1, GREEN, 2)  # рисуем зеленый отрезок толщины 2 от центра к первой точке
                cv.line(frame, center, p2, GREEN, 2)  # рисуем зеленый отрезок толщины 2 от центра ко второй точке
                cv.putText(frame, str(int(angle)), (center[0] + 20, center[1] - 20), cv.FONT_HERSHEY_SIMPLEX,
                           1, GREEN, 2)
                # пишем угол наклона, выраженный целым числом, правее и ниже на 20 пикселов,
                # шрифтом HERSHEY_SIMPLEX масштаба 1 зеленым цветом. Толщина символов 2

        # выводим итоговый кадр в окно с именем Find tilt angle
        cv.imshow('Find tilt angle', frame)

        ch = cv.waitKey(1)  # ожидаем нажатие клавиши в течение 5 мс
        if ch == ESC:  # если это была клавиша ESC, то выходим из бесконечного цикла
            break

    cpt.release()  # закрываем видеопоток
    cv.destroyAllWindows()  # закрываем все окна
